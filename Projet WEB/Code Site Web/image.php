<?php
// Spécification du type d'image que l'on va créer (format jpg)
header ("Content-type: image/jpg");
session_start();

function RandomChaine($chaine)
{

	// Shuffle du tableau
	$chaineMel = str_shuffle($chaine);
	// Extraire une partie du tableau
	$chainePart = substr($chaineMel,0,4); 
	
	return $chainePart;
}

// Création d'une image vide de 100 pixels par 30
$image = @ImageCreate (100, 30) or die ("Erreur lors de la création de l'image");

// Applique à l'image une couleur de fond, couleurs au format RGB
//$couleur_fond = ImageColorAllocate ($image, 102, 124, 148);
$fond= ImageColorAllocate ($image, 102, 124, 148);

$TableauTextes = array("RentASnow", "Snow_133", "Descentes", "SNOWNeige", "Froid_ICT"); 
$indtexte = array_rand($TableauTextes);
$texte=$TableauTextes[$indtexte]; 
$texte=RandomChaine($texte);

$_SESSION['captcha1']=$texte;

$blanc = imagecolorallocate($image, 255, 255, 255); // Définition de la couleur du texte

imagestring($image, 5, 30, 6,$texte, $blanc);       // Ecriture du texte dans l'image
// Création de l'image JPEG
Imagejpeg ($image);
?>
